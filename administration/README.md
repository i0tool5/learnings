# Administration 💻

This directory contains information as well as hacks, tricks, etc. about UNIX-like/Windows administration.

- Using [`tar` over `ssh`](./tar-over-ssh.md)
